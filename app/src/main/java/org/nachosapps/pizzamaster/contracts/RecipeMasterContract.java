package org.nachosapps.pizzamaster.contracts;

import com.facebook.CallbackManager;

import org.nachosapps.pizzamaster.activities.RecipeMaster;


public interface RecipeMasterContract {
    interface Presenter {
        void facebookLogIn(RecipeMaster activity, CallbackManager callbackManager);

        void facebookLogOut();
    }

    interface View {
        void showToast(int id);
    }

}
