package org.nachosapps.pizzamaster.activities;

import static org.nachosapps.pizzamaster.helpers.FacebookHelpers.isFbLoggedIn;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.NavigationMenu;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import org.nachosapps.pizzamaster.R;
import org.nachosapps.pizzamaster.contracts.RecipeMasterContract;
import org.nachosapps.pizzamaster.presenter.RecipeMasterPresenter;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class RecipeMaster extends AppCompatActivity implements RecipeMasterContract.View {

    private CallbackManager callbackManager;

    RecipeMasterPresenter recipeMasterPresenter = new RecipeMasterPresenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_master);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        FabSpeedDial fabSpeedDial = findViewById(R.id.fabSpeedDial);

        callbackManager = CallbackManager.Factory.create();

        invalidateOptionsMenu();

        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                @SuppressLint("RestrictedApi") MenuItem loginItem = navigationMenu.findItem(R.id
                        .action_login);
                if (isFbLoggedIn()) {
                    loginItem.setTitle(R.string.com_facebook_loginview_log_out_button
                    );
                } else {
                    loginItem.setTitle(R.string.com_facebook_loginview_log_in_button_continue);
                }
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_recipe:
                        Intent intent = new Intent(getApplicationContext(), Recipe.class);
                        startActivity(intent);
                        return true;
                    case R.id.action_login:
                        if (isFbLoggedIn()) {
                            recipeMasterPresenter.facebookLogOut();
                        } else {
                            recipeMasterPresenter.facebookLogIn(RecipeMaster.this, callbackManager);
                        }
                }
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void showToast(int id) {
        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
    }
}