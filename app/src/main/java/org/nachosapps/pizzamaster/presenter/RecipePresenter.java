package org.nachosapps.pizzamaster.presenter;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.graphics.Bitmap;

import com.artjimlop.altex.AltexImageDownloader;
import com.facebook.Profile;

import org.nachosapps.pizzamaster.R;
import org.nachosapps.pizzamaster.helpers.FacebookHelpers;
import org.nachosapps.pizzamaster.interfaces.ApiService;
import org.nachosapps.pizzamaster.contracts.RecipeContract;
import org.nachosapps.pizzamaster.model.RecipeModel;
import org.nachosapps.pizzamaster.model.RetroClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RecipePresenter implements RecipeContract.Presenter {
    private RecipeContract.View view;

    public RecipePresenter(RecipeContract.View view) {
        this.view = view;
    }

    @Override
    public void getRecipeData() {
        view.showLoading();
        ApiService api = RetroClient.getApiService();
        Call<RecipeModel> call = api.getMyJSON();

        call.enqueue(new Callback<RecipeModel>() {
            @Override
            public void onResponse(Call<RecipeModel> call, Response<RecipeModel> response) {
                if (response.isSuccessful()) {
                    view.hideLoading();
                    view.showRecipe(response.body());
                    view.showImages(response.body());
                } else {
                    view.showToast(R.string.recipe_not_available);
                    view.hideLoading();
                }
            }

            @Override
            public void onFailure(Call<RecipeModel> call, Throwable t) {
                view.showToast(R.string.error_msg);
                view.hideLoading();
            }
        });
    }

    @Override
    public void getFacebookProfile() {
        Profile profile = Profile.getCurrentProfile();
        if (FacebookHelpers.isFbLoggedIn()) {
            view.showFacebookProfile(profile);
        }
    }

    @Override
    public void saveImage(final String imageUrl) {
        final AltexImageDownloader downloader = new AltexImageDownloader(
                new AltexImageDownloader.OnImageLoaderListener() {
                    @Override
                    public void onError(AltexImageDownloader.ImageError error) {
                        view.showToast(R.string.image_not_saved);
                    }

                    @Override
                    public void onProgressChange(int percent) {
                    }

                    @Override
                    public void onComplete(Bitmap result) {
                        AltexImageDownloader.writeToDisk(getApplicationContext(),
                                imageUrl, "");
                        view.showToast(R.string.image_saved);
                    }
                });
        downloader.download(imageUrl, false);
    }
}
