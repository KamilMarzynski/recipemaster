package org.nachosapps.pizzamaster.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecipeModel {

    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("ingredients")
    private List<String> ingredients = null;
    @SerializedName("preparing")
    private List<String> preparing = null;
    @SerializedName("imgs")
    private List<String> images = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getIngredients() {
        return ingredients;
    }


    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public List<String> getPreparing() {
        return preparing;
    }

    public void setPreparing(List<String> preparing) {
        this.preparing = preparing;
    }

    public List<String> getImgs() {
        return images;
    }

    public void setImgs(List<String> images) {
        this.images = images;
    }
}