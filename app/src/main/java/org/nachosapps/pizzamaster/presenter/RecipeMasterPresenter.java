package org.nachosapps.pizzamaster.presenter;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.nachosapps.pizzamaster.R;
import org.nachosapps.pizzamaster.activities.RecipeMaster;
import org.nachosapps.pizzamaster.contracts.RecipeMasterContract;

import java.util.Collections;


public class RecipeMasterPresenter implements RecipeMasterContract.Presenter {
    private RecipeMasterContract.View view;

    public RecipeMasterPresenter(RecipeMasterContract.View view) {
        this.view = view;
    }

    @Override
    public void facebookLogIn(RecipeMaster activity, CallbackManager callbackManager) {
        LoginManager.getInstance().logInWithReadPermissions(activity,
                Collections.singletonList("public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        view.showToast(R.string.logged_in_hello);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException error) {
                        view.showToast(R.string.error_while_logging_to_fb);
                    }
                });
    }

    @Override
    public void facebookLogOut() {

        view.showToast(R.string.logging_out);
        new GraphRequest(AccessToken.getCurrentAccessToken(),
                "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
                AccessToken.setCurrentAccessToken(null);
                view.showToast(R.string.logged_out_good_bye);
            }
        }).executeAsync();
    }
}

