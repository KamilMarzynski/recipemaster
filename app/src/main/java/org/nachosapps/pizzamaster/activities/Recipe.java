package org.nachosapps.pizzamaster.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.squareup.picasso.Picasso;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;

import org.nachosapps.pizzamaster.R;
import org.nachosapps.pizzamaster.adapters.ImagesAdapter;
import org.nachosapps.pizzamaster.contracts.RecipeContract;
import org.nachosapps.pizzamaster.helpers.ExpandableHeightGridView;
import org.nachosapps.pizzamaster.helpers.RecipeHelpers;
import org.nachosapps.pizzamaster.model.RecipeModel;
import org.nachosapps.pizzamaster.presenter.RecipePresenter;

public class Recipe extends AppCompatActivity implements RecipeContract.View {

    ScrollView mainLayout;

    TextView txtIngredients;
    TextView txtPreparing;
    TextView txtTitle;
    TextView txtDescription;
    TextView ingredientsHeader;
    TextView preparingHeader;
    TextView imagesHeader;

    RelativeLayout profileLayout;
    TextView profileName;
    ImageView profileImg;

    ExpandableHeightGridView imagesView;

    ProgressBar progressBar;

    String imageUrl;

    RecipePresenter recipePresenter = new RecipePresenter(this);

    public static int PERMISSION_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initViews();

        recipePresenter.getRecipeData();
        recipePresenter.getFacebookProfile();
    }

    @Override
    public void showRecipe(final RecipeModel recipeData) {
        TransitionManager.beginDelayedTransition(mainLayout, new Slide().setDuration(250));
        
        if (recipeData.getTitle() != null) {
            String title = recipeData.getTitle();
            txtTitle.setText(title + ":");
            getSupportActionBar().setTitle(title + getString(R.string.actionbar_title));
            setViewsVisible(txtTitle);
        }

        if (recipeData.getDescription() != null) {
            String description = recipeData.getDescription();
            txtDescription.setText(description);
            setViewsVisible(txtDescription);
        }

        if (recipeData.getIngredients() != null) {
            String ingredients = RecipeHelpers.getIngredientsString(recipeData.getIngredients());
            txtIngredients.setText(ingredients);
            setViewsVisible(txtIngredients, ingredientsHeader);
        }

        if (recipeData.getPreparing() != null) {
            String preparing = RecipeHelpers.getPreparingString(recipeData.getPreparing());
            txtPreparing.setText(preparing);
            setViewsVisible(txtPreparing, preparingHeader);
        }
    }

    @Override
    public void showImages(final RecipeModel recipeData) {
        if (recipeData.getImgs() != null) {
            TransitionManager.beginDelayedTransition(mainLayout, new Slide().setDuration(250));
            setViewsVisible(imagesView, imagesHeader);
            imagesView.setAdapter(new ImagesAdapter(this, recipeData.getImgs()));
            imagesView.setExpanded(true);
            imagesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                        int position, long id) {
                    imageUrl = recipeData.getImgs().get(position);
                    showSaveAlert();
                }
            });
        }
    }

    @Override
    public void showFacebookProfile(Profile profile) {
        TransitionManager.beginDelayedTransition(mainLayout, new Slide().setDuration(250));
        setViewsVisible(profileLayout);
        String profileText = getString(R.string.logged_as) + profile.getName();
        profileName.setText(profileText);
        Picasso.get().load(profile.getProfilePictureUri(250, 250)).into(profileImg);
    }

    @Override
    public void showSaveAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirm);
        builder.setMessage(R.string.want_to_save_picture);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (checkPermission()) {
                    recipePresenter.saveImage(imageUrl);
                }
                dialog.dismiss();
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                showToast(R.string.image_not_saved);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }


    @Override
    public void showToast(int id) {
        Toast.makeText(this, id,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            recipePresenter.saveImage(imageUrl);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void initViews() {
        mainLayout = findViewById(R.id.recipeLayout);
        txtTitle = findViewById(R.id.recipeTitle);
        txtDescription = findViewById(R.id.description);
        txtIngredients = findViewById(R.id.ingredients);
        txtPreparing = findViewById(R.id.preparing);
        preparingHeader = findViewById(R.id.preparing_header);
        ingredientsHeader = findViewById(R.id.ingredients_header);
        imagesHeader = findViewById(R.id.images_header);

        profileLayout = findViewById(R.id.profileLayout);
        profileImg = findViewById(R.id.profileImage);
        profileName = findViewById(R.id.profileName);

        progressBar = findViewById(R.id.progressBar);

        imagesView = findViewById(R.id.gridview);
        setViewsInvisible(txtTitle, txtDescription, txtIngredients, txtPreparing, profileLayout,
                imagesView, preparingHeader, ingredientsHeader, imagesHeader);
    }

    private void setViewsVisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }


    private void setViewsInvisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.INVISIBLE);
        }
    }


    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showToast(R.string.permission_needed);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_CODE);
            }
        } else {
            return true;
        }
        return false;
    }
}
