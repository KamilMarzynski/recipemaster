package org.nachosapps.pizzamaster.helpers;

import com.facebook.AccessToken;


public class FacebookHelpers {
    public static boolean isFbLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
}
