package org.nachosapps.pizzamaster.contracts;


import com.facebook.Profile;

import org.nachosapps.pizzamaster.model.RecipeModel;


public interface RecipeContract {
    interface Presenter {
        void getRecipeData();

        void getFacebookProfile();

        void saveImage(String imageUrl);
    }

    interface View {
        void showRecipe(RecipeModel recipeData);

        void showImages(RecipeModel recipeData);

        void showFacebookProfile(Profile profile);

        void showLoading();

        void hideLoading();

        void showSaveAlert();

        void showToast(int id);
    }
}
