package org.nachosapps.pizzamaster.helpers;

import java.util.List;


public class RecipeHelpers {

    public static String getIngredientsString(List<String> ingredients) {
        StringBuilder ingredientsString = new StringBuilder();
        for (int i = 0; i < ingredients.size(); i++) {
            ingredientsString.append("-").append(ingredients.get(i)).append("\n");
        }
        return ingredientsString.toString();
    }

    public static String getPreparingString(List<String> preparing) {
        StringBuilder sbPreparing = new StringBuilder();
        for (int i = 0; i < preparing.size(); i++) {
            sbPreparing.append(String.format("%d. ", i + 1)).append(preparing.get(i)).append
                    ("\n").append("\n");
        }
        return sbPreparing.toString();
    }
}
