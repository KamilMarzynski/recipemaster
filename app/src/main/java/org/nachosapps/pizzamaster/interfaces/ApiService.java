package org.nachosapps.pizzamaster.interfaces;

import org.nachosapps.pizzamaster.model.RecipeModel;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiService {
    @GET("/test/info.php")
    Call<RecipeModel> getMyJSON();
}
